import dotenv from "dotenv";
dotenv.config();
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import router from "./routes";
import swaggerUi from "swagger-ui-express";
import yamljs from "yamljs";
const swaggerDoc = yamljs.load("swagger.yaml");
import jwt from "./config/jwt";

export default class Server {

  static config() {
    const app = express();
    app.use(jwt());

    app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDoc))
    app.use("/uploads", express.static("uploads"));

    // Configuration de l"app
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(cors({origin: true}));
    app.use("/", router);

    return app;
  }
}
