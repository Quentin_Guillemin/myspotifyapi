import { Schema, model } from "mongoose";

const PlaylistSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  public: {
    type: Boolean,
    default: false
  },
  loved_playlist: {
    type: Boolean,
    default: false
  },
  songs: [{
    type: Schema.Types.ObjectId,
    ref: "Song",
  }],
  creator: {
    type: Schema.Types.ObjectId,
    ref: "User",
  }
});

export default model("Playlist", PlaylistSchema);
