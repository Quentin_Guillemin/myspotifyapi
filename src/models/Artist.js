import { Schema, model } from "mongoose";

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  bio: {
    type: String,
    default: null
  },
  profile_photo: {
    type: String,
    default: null
  },
  albums: [{
    type: Schema.Types.ObjectId,
    ref: "Album",
  }]
});

export default model("Artist", ArtistSchema);
