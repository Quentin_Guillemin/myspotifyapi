import { Schema, model } from "mongoose";

const AlbumSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  year: {
    type: Date,
    required: true
  },
  cover: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  songs: [{
    type: Schema.Types.ObjectId,
    ref: "Song",
  }],
  genres: [{
    type: Schema.Types.ObjectId,
    ref: "Genre",
  }]
});

export default model("Album", AlbumSchema);
