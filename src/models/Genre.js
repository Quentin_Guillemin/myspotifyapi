import { Schema, model } from "mongoose";

const GenreSchema = new Schema({
  libelle: {
    type: String,
    required: true,
    unique: true
  }
});

export default model("Genre", GenreSchema);
