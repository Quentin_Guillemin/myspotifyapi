import { Schema, model } from "mongoose";

const SongSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  year: {
    type: Date,
    required: true
  },
  track_num: {
    type: Number,
    default: null
  },
  nb_listen: {
    type: Number,
    default: 0
  },
  file_location: {
    type: String,
    required: true
  },
  genres: [{
    type: Schema.Types.ObjectId,
    ref: "Genre"
  }],
  artists: [{
    type: Schema.Types.ObjectId,
    ref: "Artist"
  }]
});

export default model("Song", SongSchema);
