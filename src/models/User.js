import { Schema, model } from "mongoose";

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  role: {
    type: Number,
    default: 0
  },
  password: {
    type: String,
    required: true
  },
  playlists: [{
    type: Schema.Types.ObjectId,
    ref: "Playlist",
  }],
  albums: [{
    type: Schema.Types.ObjectId,
    ref: "Album",
  }],
  artists: [{
    type: Schema.Types.ObjectId,
    ref: "Artist",
  }]
});

export default model("User", UserSchema);
