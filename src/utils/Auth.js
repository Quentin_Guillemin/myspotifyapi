import jsonwebtoken from "jsonwebtoken";
import Playlist from "../models/Playlist";
import User from "../models/User";

export default class Auth {

  static isAllowed(roles) {
    return async (req, res, next) => {
      try {
        let token = req.headers.authorization.replace(/Bearer /g, "");
        let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
        let user = await User.findById(decryptToken.sub);

        if (roles.includes("self_user")) {
          let { id } = req.params;
          if (user._id == id)
            next();
          else
            return res.status(401).json({ message: "Unauthorized" });
        }

        else if (roles.includes("self_playlists")) {
          let { id } = req.params;
          let playlist = await Playlist.findById(id);

          if (playlist.creator == user._id)
            next();
          else
            return res.status(401).json({ message: "Unauthorized" });
        }

        else if (roles.includes(user.role))
          next();
        else
          return res.status(401).json({ message: "Unauthorized" });
      } catch (e) {
        return res.status(403).json({ message: "Missing token" });
      }
    }
  }

}
