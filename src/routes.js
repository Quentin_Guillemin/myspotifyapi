import { Router } from "express";
const router = Router();
import SearchController from "./controllers/SearchController";
import SongController from "./controllers/SongController";
import AlbumController from "./controllers/AlbumController";
import ArtistController from "./controllers/ArtistController";
import PlaylistController from "./controllers/PlaylistController";
import GenreController from "./controllers/GenreController";
import UserController from "./controllers/UserController";
import Auth from "./utils/Auth";
import Multer from "./utils/Multer";

/*
 * Search
 */
router.get("/search/:search", SearchController.search);

/*
 * Songs
 */
router.post("/songs", Auth.isAllowed([10]), Multer.upload("songs", "file_location"), SongController.store);
router.get("/songs/:id", SongController.isLoved);
router.put("/songs/:id", Auth.isAllowed([10]), SongController.update);
router.delete("/songs/:id", Auth.isAllowed([10]), SongController.remove);
router.put("/songs/:id/file", Auth.isAllowed([10]), Multer.upload("songs", "file_location"), SongController.updateFile);

/*
 * Albums
 */
router.get("/albums", AlbumController.list);
router.post("/albums", Auth.isAllowed([10]), Multer.upload("albums_cover", "cover"), AlbumController.store);
router.get("/albums/:id", AlbumController.details);
router.put("/albums/:id", Auth.isAllowed([10]), AlbumController.update);
router.delete("/albums/:id", Auth.isAllowed([10]), AlbumController.remove);
router.put("/albums/:id/cover", Auth.isAllowed([10]), Multer.upload("albums_cover", "cover"), AlbumController.updateCover);
router.put("/albums/:id/songs/:songId", Auth.isAllowed([10]), AlbumController.addSong);
router.delete("/albums/:id/songs/:songId", Auth.isAllowed([10]), AlbumController.removeSong);

/*
 * Artists
 */
router.get("/artists", ArtistController.list);
router.post("/artists", Auth.isAllowed([10]), Multer.upload("artists_photos", "profile_photo"), ArtistController.store);
router.get("/artists/:id", ArtistController.details);
router.put("/artists/:id", Auth.isAllowed([10]), ArtistController.update);
router.delete("/artists/:id", Auth.isAllowed([10]), ArtistController.remove);
router.put("/artists/:id/profile_photo", Auth.isAllowed([10]), Multer.upload("artists_photos", "profile_photo"), ArtistController.updateProfilePhoto);
router.put("/artists/:id/albums/:albumId", Auth.isAllowed([10]), ArtistController.addAlbum);
router.delete("/artists/:id/albums/:albumId", Auth.isAllowed([10]), ArtistController.removeAlbum);

/*
 * Playlists
 */
router.get("/playlists", PlaylistController.list);
router.post("/playlists", PlaylistController.store);
router.get("/playlists/:id", PlaylistController.details);
router.put("/playlists/:id", Auth.isAllowed([10, "self_playlists"]), PlaylistController.update);
router.delete("/playlists/:id", Auth.isAllowed([10, "self_playlists"]), PlaylistController.remove);
router.put("/playlists/:id/songs/:songId", Auth.isAllowed([10, "self_playlists"]), PlaylistController.addSong);
router.delete("/playlists/:id/songs/:songId", Auth.isAllowed([10, "self_playlists"]), PlaylistController.removeSong);

/*
 * Genres
 */
router.get("/genres", GenreController.list);
router.post("/genres", Auth.isAllowed([10]), GenreController.store);
router.get("/genres/:id", GenreController.getSongs);
router.put("/genres/:id", Auth.isAllowed([10]), GenreController.update);
router.delete("/genres/:id", Auth.isAllowed([10]), GenreController.remove);

/*
 * Users
 */
router.get("/users", Auth.isAllowed([10]), UserController.list);
router.post("/users", UserController.store);
router.get("/users/:id", Auth.isAllowed(["self_user"]), UserController.details);
router.put("/users/:id", Auth.isAllowed(["self_user"]), UserController.update);
router.delete("/users/:id", Auth.isAllowed([10, "self_user"]), UserController.remove);
router.put("/users/:id/playlists/:playlistId", Auth.isAllowed(["self_user"]), UserController.addPlaylist);
router.delete("/users/:id/playlists/:playlistId", Auth.isAllowed(["self_user"]), UserController.removePlaylist);
router.put("/users/:id/albums/:albumId", Auth.isAllowed(["self_user"]), UserController.addAlbum);
router.delete("/users/:id/albums/:albumId", Auth.isAllowed(["self_user"]), UserController.removeAlbum);
router.put("/users/:id/artists/:artistId", Auth.isAllowed(["self_user"]), UserController.addArtist);
router.delete("/users/:id/artists/:artistId", Auth.isAllowed(["self_user"]), UserController.removeArtist);
router.put("/users/:id/loved/:songId", Auth.isAllowed(["self_user"]), UserController.addSongToLovedPlaylist);
router.delete("/users/:id/loved/:songId", Auth.isAllowed(["self_user"]), UserController.removeSongOfLovedPlaylist);
router.post("/users/auth", UserController.auth);

export default router;
