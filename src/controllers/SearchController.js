import Album from "../models/Album";
import Artist from "../models/Artist";
import Playlist from "../models/Playlist";
import Song from "../models/Song";
import Utils from "../utils/Utils";

export default class SongController {

  static async search(req, res) {
    return Utils.process(async (req, res) => {
      let { search } = req.params;
      let albums = await Album.find({ name: { $regex: `^${search}` } }).select("-__v -songs -description -genres");
      let artists = await Artist.find({ name: { $regex: `^${search}` } }).select("-__v -bio -albums");
      let songs = await Song.find({ title: { $regex: `^${search}` } })
        .populate({ path: "genres", select: "_id libelle" })
        .populate({ path: "artists", select: "_id name" })
        .select("-__v -track_num -nb_listen");
      let playlists = await Playlist.find({ name: { $regex: `^${search}` }, public: true, loved_playlist: false })
        .select("-__v -loved_playlist -public -songs");

      return { albums, artists, songs, playlists };
    }, req, res);
  }

}
