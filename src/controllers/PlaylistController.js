import jsonwebtoken from "jsonwebtoken";
import Playlist from "../models/Playlist";
import User from "../models/User";
import Utils from "../utils/Utils";

export default class PlaylistController {

  static async list(req, res) {
    return Utils.process(async (req, res) => {
      let { limit, skip } = req.query;
      let playlists;
      if (limit & skip) {
        playlists = await Playlist.find({ public: true, loved_playlist: false })
          .limit(parseInt(limit))
          .skip(parseInt(skip))
          .select("-__v -loved_playlist -public -songs");
      } else {
        playlists = await Playlist.find({ public: true, loved_playlist: false })
          .select("-__v -loved_playlist -public -songs");
      }

      return { playlists }
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let playlist = await Playlist.create(req.body);
      if (playlist) {
        playlist = await Playlist.findById(playlist._id)
          .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
          .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
          .select("-__v");
        let token = req.headers.authorization.replace(/Bearer /g, "");
        let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
        let user = await User.findById(decryptToken.sub);
        user.playlists.push(playlist._id);
        user.save();
      }

      return { playlist };
    }, req, res);
  }

  static async details(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let playlist = await Playlist.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");

      let token = req.headers.authorization.replace(/Bearer /g, "");
      let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
      let user = await User.findById(decryptToken.sub);

      let loved = false;
      if (user.playlists.indexOf(id) > -1)
        loved = true;

      return { playlist, loved };
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let playlist = await Playlist.findByIdAndUpdate(id, req.body, { new: true })
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");

      return { playlist };
    }, req, res);
  }

  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let playlist = await Playlist.findById(id);
      if (playlist && !playlist.loved_playlist) {
        let users = await User.find({ playlists: id }).select("-__v -password");
        users.forEach(async user => {
          let index = user.playlists.indexOf(id);
          if (index > -1) {
            user.playlists.splice(index, 1);
            await user.save();
          }
        });
        await playlist.delete();
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addSong(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let playlist = await Playlist.findById(id).select("-__v");
      let song = await Song.findById(songId);
      let index = playlist.songs.indexOf(songId);
      if (playlist && song && index == -1) {
        playlist.songs.push(songId);
        await playlist.save();
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async removeSong(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let playlist = await Playlist.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");
      let index = playlist.songs.indexOf(songId);
      if (index > -1) {
        playlist.songs.splice(index, 1);
        await playlist.save();

        return { playlist };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

}
