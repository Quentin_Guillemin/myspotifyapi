import Genre from "../models/Genre";
import Utils from "../utils/Utils";
import Playlist from "../models/Playlist";
import Song from "../models/Song";

export default class GenreController {

  static async list(req, res) {
    return Utils.process(async (req, res) => {
      let genres = await Genre.find().select("-__v");

      return { genres };
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let genre = await Genre.create(req.body);

      return { genre };
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let genre = await Genre.findByIdAndUpdate(id, req.body, { new: true }).select("-__v");

      return { genre };
    }, req, res);
  }

  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let genre = await Genre.findByIdAndDelete(id);
      if (!genre) {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async getSongs(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let genre = await Genre.findById(id).select("-__v");
      if (genre) {
        let songs = await Song.find({ genres: id })
          .populate({ path: "artists", select: "_id name" })
          .populate({ path: "genres", select: "_id libelle" })
          .select("-__v -track_num");

        return { genre, songs };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

}
