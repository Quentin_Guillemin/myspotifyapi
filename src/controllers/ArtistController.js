import Album from "../models/Album";
import Artist from "../models/Artist";
import fs from "fs";
import jsonwebtoken from "jsonwebtoken";
import User from "../models/User";
import Utils from "../utils/Utils";

export default class ArtistController {

  static async list(req, res) {
    return Utils.process(async (req, res) => {
      let { limit, skip } = req.query;
      let artists;
      if (limit & skip) {
        artists = await Artist.find()
          .limit(parseInt(limit))
          .skip(parseInt(skip))
          .select("-__v -bio");
      } else {
        artists = await Artist.find().select("-__v -bio");
      }

      return { artists };
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let artist = await Artist.create(req.body);
      artist = await Artist.findById(artist._id)
        .populate({ path: "albums", select: "-__v -songs -description -genres" })
        .select("-__v");

      return { artist };
    }, req, res);
  }

  static async details(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let artist = await Artist.findById(id)
        .populate({ path: "albums", select: "-__v -songs -description -genres" })
        .select("-__v");

      let token = req.headers.authorization.replace(/Bearer /g, "");
      let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
      let user = await User.findById(decryptToken.sub);

      let loved = false;
      if (user.artists.indexOf(id) > -1)
        loved = true;

      return { artist, loved };
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      delete req.body.profile_photo;
      let artist = await Artist.findByIdAndUpdate(id, req.body, { new: true })
        .populate({ path: "albums", select: "-__v -songs -description -genres" })
        .select("-__v");

      return { artist };
    }, req, res);
  }

  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let artist = await Artist.findById(id);
      if (artist) {
        let users = await User.find({ artists: id }).select("-__v -password");
        users.forEach(async user => {
          let index = user.artists.indexOf(id);
          if (index > -1) {
            user.artists.splice(index, 1);
            await user.save();
          }
        });
        artist.albums.forEach(async album => {
          album.songs.forEach(async song => {
            await fs.unlinkSync(`./${song.file_location}`);
          });
          await fs.unlinkSync(`./${album.cover}`);
        });
        await artist.delete();
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async updateProfilePhoto(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let artist = await Artist.findById(id).select("-__v");
      if (artist) {
        if (fs.existsSync(`./${artist.profile_photo}`))
          await fs.unlinkSync(`./${artist.profile_photo}`);
        artist.profile_photo = req.body.profile_photo;
        await artist.save();
        artist = await findById(artist._id)
          .populate({ path: "albums", select: "-__v -songs -description -genres" })
          .select("-__v");

        return { artist };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addAlbum(req, res) {
    return Utils.process(async (req, res) => {
      let { id, albumId } = req.params;
      let artist = await Artist.findById(id).select("-__v");
      let album = await Album.findById(albumId);
      let index = artist.albums.indexOf(albumId);
      if (artist && album && index == -1) {
        artist.albums.push(albumId);
        await artist.save();
        artist = await findById(artist._id)
          .populate({ path: "albums", select: "-__v -songs -description -genres" })
          .select("-__v");

        return { artist };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async removeAlbum(req, res) {
    return Utils.process(async (req, res) => {
      let { id, albumId } = req.params;
      let artist = await Artist.findById(id).select("-__v");
      let index = artist.albums.indexOf(albumId);
      if (index > -1) {
        artist.albums.splice(index, 1);
        await artist.save();
        artist = await findById(artist._id)
          .populate({ path: "albums", select: "-__v -songs -description -genres" })
          .select("-__v");

        return { artist };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

}
