import Album from "../models/Album";
import fs from "fs";
import jsonwebtoken from "jsonwebtoken";
import Song from "../models/Song";
import User from "../models/User";
import Utils from "../utils/Utils";

export default class AlbumController {

  static async list(req, res) {
    return Utils.process(async (req, res) => {
      let { limit, skip } = req.query;
      let albums 
      if (limit & skip) {
        albums = await Album.find()
          .limit(parseInt(limit))
          .skip(parseInt(skip))
          .populate({ path: "genres", select: "_id name" })
          .select("-__v -songs -description -genres");
      } else {
        albums = await Album.find()
          .populate({ path: "genres", select: "_id name" })
          .select("-__v -songs -description -genres");
      }

      return { albums };
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let album = await Album.create(req.body);
      album = await Album.findById(album._id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");

      return { album };
    }, req, res);
  }

  static async details(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let album = await Album.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");

      let token = req.headers.authorization.replace(/Bearer /g, "");
      let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
      let user = await User.findById(decryptToken.sub);

      let loved = false;
      if (user.albums.indexOf(id) > -1)
        loved = true;

      return { album, loved };
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      delete req.body.cover;
      let album = await Album.findByIdAndUpdate(id, req.body, { new: true })
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");

      return { album };
    }, req, res);
  }
  
  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let album = await Album.findById(id);
      if (album) {
        let users = await User.find({ albums: id }).select("-__v -password");
        users.forEach(async user => {
          let index = user.albums.indexOf(id);
          if (index > -1) {
            user.albums.splice(index, 1);
            await user.save();
          }
        });
        album.songs.forEach(async song => {
          await fs.unlinkSync(`./${song.file_location}`);
        });
        await fs.unlinkSync(`./${album.cover}`);
        await album.delete();
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async updateCover(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let album = await Album.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");
      if (album) {
        if (fs.existsSync(`./${album.cover}`))
          await fs.unlinkSync(`./${album.cover}`);
        album.cover = req.body.cover;
        await album.save();

        return { album };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addSong(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let album = await Album.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");
      let song = await Song.findById(songId);
      let index = album.songs.indexOf(songId);
      if (album && song && index == -1) {
        album.songs.push(songId);
        await album.save();

        return { album };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async removeSong(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let album = await Album.findById(id)
        .populate({ path: "songs", select: "-__v", populate: { path: "artists", select: "_id name" } })
        .populate({ path: "songs", select: "-__v", populate: { path: "genres", select: "_id libelle" } })
        .select("-__v");
      let index = album.songs.indexOf(songId);
      if (index > -1) {
        album.songs.splice(index, 1);
        await album.save();

        return { album };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

}
