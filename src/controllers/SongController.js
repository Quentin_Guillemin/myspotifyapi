import fs from "fs";
import jsonwebtoken from "jsonwebtoken";
import Playlist from "../models/Playlist";
import Song from "../models/Song";
import User from "../models/User";
import Utils from "../utils/Utils";

export default class SongController {

  static async isLoved(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;

      let token = req.headers.authorization.replace(/Bearer /g, "");
      let decryptToken = jsonwebtoken.decode(token, process.env.JWT_SECRET);
      let user = await User.findById(decryptToken.sub);

      let loved = false;
      let lovedPlaylist = await Playlist.findOne({ creator: user._id, loved_playlist: true });
      if (lovedPlaylist.songs.indexOf(id) > -1)
        loved = true;

      return { loved };
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let song = await Song.create(req.body);
      song = await Song.findById(song._id)
        .populate({ path: "artists", select: "_id name" })
        .populate({ path: "genres", select: "_id libelle" })
        .select("-__v");

      return { song };
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      delete req.body.file_location;
      let song = await Song.findByIdAndUpdate(id, req.body, { new: true })
        .populate({ path: "artists", select: "_id name" })
        .populate({ path: "genres", select: "_id libelle" })
        .select("-__v");

      return { song };
    }, req, res);
  }

  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let song = await Song.findByIdAndDelete(id);
      if (song) {
        if (fs.existsSync(`./${song.file_location}`))
          await fs.unlinkSync(`./${song.file_location}`);
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async updateFile(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let song = await Song.findById(id)
        .populate({ path: "artists", select: "_id name" })
        .populate({ path: "genres", select: "_id libelle" })
        .select("-__v");
      if (song) {
        if (fs.existsSync(`./${song.file_location}`))
          await fs.unlinkSync(`./${song.file_location}`);
        song.file_location = req.body.file_location;
        await song.save();
  
        return { song };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

}
