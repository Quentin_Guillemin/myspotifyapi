import Album from "../models/Album";
import Artist from "../models/Artist";
import jsonwebtoken from "jsonwebtoken"
import Playlist from "../models/Playlist";
import Song from "../models/Song";
import User from "../models/User";
import Utils from "../utils/Utils";

export default class UserController {

  static async list(req, res) {
    return Utils.process(async (req, res) => {
      let { limit, skip } = req.query;
      let users;
      if (limit & skip) {
        users = await User.find()
          .limit(parseInt(limit))
          .skip(parseInt(skip))
          .select("-__v -password -playlists -albums -artists");
      } else {
        users = await User.find().select("-__v -password -playlists -albums -artists");
      }

      return { users };
    }, req, res);
  }

  static async details(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let user = await User.findById(id)
        .populate({ path: "playlists", select: "_id name loved_playlist" })
        .populate({ path: "albums", select: "_id name cover year" })
        .populate({ path: "artists", select: "_id name profile_photo" })
        .select("-__v -password");

      return { user };
    }, req, res);
  }

  static async store(req, res) {
    return Utils.process(async (req, res) => {
      let user = await User.create(req.body);
      if (user) {
        let lovedPlaylist = await Playlist.create({ name: "loved", loved_playlist: true, creator: user._id});
        user.playlists.push(lovedPlaylist._id);
        await user.save();
        user = await User.findOne({ "email": user.email })
          .select("-__v -password -artists -albums -playlists");

        let { JWT_SECRET } = process.env;
        let token = jsonwebtoken.sign({ sub: user._id }, JWT_SECRET);

        return { user, token };
      }
    }, req, res);
  }

  static async update(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let user = await User.findByIdAndUpdate(id, req.body, { new: true })
        .populate({ path: "playlists", select: "_id name" })
        .populate({ path: "albums", select: "_id name cover year" })
        .populate({ path: "artists", select: "_id name profile_photo" })
        .select("-__v -password");

      return { user };
    }, req, res);
  }

  static async remove(req, res) {
    return Utils.process(async (req, res) => {
      let { id } = req.params;
      let user = await User.findById(id);
      if (user) {
        let playlists = await Playlist.find({ creator: user._id }).select("-__v");
        playlists.forEach(async playlist => {
          let users = await User.find({ playlists: playlists._id }).select("-__v -password");
          users.forEach(async user => {
            let index = user.playlists.indexOf(id);
            if (index > -1) {
              user.playlists.splice(index, 1);
              await user.save();
            }
          });
          await playlist.delete();
        });
        await user.delete();
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addPlaylist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, playlistId } = req.params;
      let user = await User.findById(id).select("-__v -password");
      let playlist = await Playlist.findById(playlistId);
      let index = user.playlists.indexOf(playlistId);
      if (user && playlist && index == -1) {
        user.playlists.push(playlistId);
        await user.save();
      } else {
        new Error("Already in the collection");
        res.status(500).json();
      }
    }, req, res);
  }

  static async removePlaylist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, playlistId } = req.params;
      let user = await User.findById(id).select("-__v -password");
      let playlist = await Playlist.findById(playlistId);
      let index = user.playlists.indexOf(playlistId);
      if (playlist && index > -1 && !playlist.loved_playlist && playlist.public) {
        user.playlists.splice(index, 1);
        await user.save();

        return { playlists: user.playlists };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addAlbum(req, res) {
    return Utils.process(async (req, res) => {
      let { id, albumId } = req.params;
      let user = await User.findById(id)
        .select("-__v -password");
      let album = await Album.findById(albumId);
      let index = user.albums.indexOf(albumId);
      if (user && album && index == -1) {
        user.albums.push(albumId);
        await user.save();
      } else {
        new Error("Already in the collection");
        res.status(500).json();
      }
    }, req, res);
  }

  static async removeAlbum(req, res) {
    return Utils.process(async (req, res) => {
      let { id, albumId } = req.params;
      let user = await User.findById(id)
        .select("-__v -password");
      let index = user.albums.indexOf(albumId);
      if (index > -1) {
        user.albums.splice(index, 1);
        await user.save();

        return { albums: user.albums };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addArtist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, artistId } = req.params;
      let user = await User.findById(id)
        .select("-__v -password");
      let artist = await Artist.findById(artistId);
      let index = user.artists.indexOf(artistId);
      if (user && artist && index == -1) {
        user.artists.push(artistId);
        await user.save();
      } else {
        new Error("Already in the collection");
        res.status(500).json();
      }
    }, req, res);
  }

  static async removeArtist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, artistId } = req.params;
      let user = await User.findById(id)
        .select("-__v -password");
      let index = user.artists.indexOf(artistId);
      if (index > -1) {
        user.artists.splice(index, 1);
        await user.save();

        return { artists: user.artists };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async addSongToLovedPlaylist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let song = await Song.findById(songId);
      let lovedPlaylist = await Playlist.findOne({ creator: id, loved_playlist: true });
      let index = lovedPlaylist.songs.indexOf(songId);
      if (song && index == -1) {
        lovedPlaylist.songs.push(songId);
        await lovedPlaylist.save();
      } else {
        new Error("Already in the collection");
        res.status(500).json();
      }
    }, req, res);
  }

  static async removeSongOfLovedPlaylist(req, res) {
    return Utils.process(async (req, res) => {
      let { id, songId } = req.params;
      let lovedPlaylist = await Playlist.findOne({ creator: id, loved_playlist: true })
        .select("-__v -loved_playlist -public -creator");
      let index = lovedPlaylist.songs.indexOf(songId);
      if (index > -1) {
        lovedPlaylist.songs.splice(index, 1);
        await lovedPlaylist.save();

        return { lovedPlaylist };
      } else {
        new Error("Not Found");
        res.status(404).json();
      }
    }, req, res);
  }

  static async auth(req, res) {
    return Utils.process(async (req, res) => {
      let { email, password } = req.body;
      let user = await User.findOne({ "email": email, "password": password })
        .select("-__v -password -artists -albums -playlists");
      if (user) {
        let { JWT_SECRET } = process.env;
        let token = jsonwebtoken.sign({ sub: user._id }, JWT_SECRET);

        return { user, token };
      } else {
        new Error("Unauthorized");
        res.status(401).json();
      }
    }, req, res);
  }

}
