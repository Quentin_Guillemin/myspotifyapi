import Database from './src/Database';
import Server from './src/Server';

const app = Server.config();

const { APP_PORT, DB_NAME, DB_HOST, DB_PORT } = process.env;

Database.init(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`)
  .then(() => {
    app.listen(APP_PORT, () => {
      console.log(`La base de données est connectée sur le port ${DB_PORT}\nLe serveur est connecté sur le port ${APP_PORT}`);
    });
  });

export default app;
